from aiogram import Bot, Dispatcher, types, executor
import requests

TEL_TOKEN = '5904290157:AAHWo2uTfwEr1_aGkkwatVjKjCYOMk-aXlI'
bot = Bot(TEL_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    await message.reply(
        "Weather bot: Enter city and you can see weather in it!"
    )


def weather(city):
    key = "848e6252641b2430903a838b9c2b6b8a"
    weather_api = f'https://api.openweathermap.org/data/2.5/weather?q={city}&APPID={key}'
    response = requests.get(weather_api).json()
    try:
        response = (f"Country: {response['sys']['country']}\n"
                    f"City: {response['name']}\n"
                    f"Weather: {response['weather'][0]['main']}\n"
                    f"Temperature: {round((response['main']['temp'] - 273.15), 2)}\n"
                    )
        return response
    except KeyError:
        return f"'{city}' - doesn't find"


@dp.message_handler()
async def today(message: types.Message):
    answer_message = weather(message.text)
    await message.answer(answer_message)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
